maskAndFormatSsn(event) {
    let ssn = event.target.value;
    let maskedSsn = '';
    maskedSsn = new Array(ssn.length-3).join('x') + ssn.substr(ssn.length-4, 4);
    maskedSsn = maskedSsn.substring(0,3)+'-'+maskedSsn.substring(3,5)+'-'+maskedSsn.substring(5,9);
    this.ssnUnmasked = ssn;
    this.ssnMaskedValue = maskedSsn;
}